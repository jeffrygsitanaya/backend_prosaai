FROM python:3.7-buster

WORKDIR /backend_prosaai

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

CMD [ "python", "test1.py" ]
