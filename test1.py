from flask import Flask, render_template, request
import pandas as pd

app = Flask(__name__)

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/get")
def get_bot_response():
    userText = request.args.get('msg')
    if str(userText) == 'STOP':
        return ('Anda Tidak Aktif. Refresh Website Lagi')
    else:
        try:
            df = pd.read_excel('/backend_prosaai/database.xlsx')
            index_match = df.loc[df['Q'].str.upper() == userText.upper()]
            ans = df.iloc[index_match.index.values[0],1]
        except Exception as e:
            ans = 'Maaf, saya tidak paham, bisa diulang lagi?'
        return (ans)

if __name__ == "__main__":

    app.run(host="0.0.0.0",port="80")
